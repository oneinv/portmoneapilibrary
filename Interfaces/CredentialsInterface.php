<?php


namespace PortmoneLib;


interface CredentialsInterface
{
    /**
     * @return array
     */
    public function getCredentials(): array;
}