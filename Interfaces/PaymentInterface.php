<?php

namespace PortmoneLib;


interface PaymentInterface
{
    /**
     * @param string $method
     * @param array $data
     * @return ResponseAbstractInterface
     */
    public function send(string $method, array $data): ResponseAbstractInterface;
}