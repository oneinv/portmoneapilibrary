<?php

namespace PortmoneLib;

interface RequestInterface
{
    /**
     * @param array $data
     * @return RequestInterface
     */
    public function fillRequest(array $data): RequestInterface;

    /**
     * @param bool $verbose
     * @return RequestInterface
     */
    public function sendRequest(bool $verbose): RequestInterface;

    /**
     * @return ResponseAbstractInterface
     */
    public function getResponse(): ResponseAbstractInterface;

}