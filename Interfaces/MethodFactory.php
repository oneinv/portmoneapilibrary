<?php

namespace PortmoneLib;

interface MethodFactory
{
    /**
     * @param string $method
     * @param CredentialsInterface $credentials
     * @return RequestInterface
     */
    public function createRequestObject(string $method, CredentialsInterface $credentials): RequestInterface;
}