<?php

namespace PortmoneLib;

interface ResponseInterface extends ResponseAbstractInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @return \stdClass
     */
    public function getExtendData(): \stdClass;
}