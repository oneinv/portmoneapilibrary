<?php

namespace PortmoneLib;

use GuzzleHttp\Client;

class InvoiceRequest implements RequestInterface
{
    public const ACTION = 'getLinkInvoice';

    private const API_URL = 'https://www.portmone.com.ua/r3/api/gateway/';
    
    /**
     * @var Client 
     */
    private $client;
    /**
     * @var CredentialsInterface 
     */
    private $credentials;

    /**
     * @var array
     */
    private $data;

    private $response;

    /**
     * InvoiceRequest constructor.
     * @param CredentialsInterface $credentials
     */
    public function __construct(CredentialsInterface $credentials)
    {
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);

        $this->credentials = $credentials;
    }

    /**
     * @param bool $verbose
     * @return RequestInterface
     * @throws PortmoneException
     */
    public function sendRequest(bool $verbose): RequestInterface
    {
        $response = $this->client->post(
            self::API_URL,
            [
                'body' => json_encode($this->data)
            ]
        );

        $this->response = new InvoiceResponse($response, $verbose);

        return $this;
    }

    /**
     * @param array $data
     * @return InvoiceRequest
     */
    public function fillRequest(array $data): RequestInterface
    {
        $this->data =  [
            'method' => self::ACTION,
            'params' => [
                'data' => [
                    'login' => $this->credentials->getLogin(),
                    'password' => $this->credentials->getPassword(),
                    'payeeId' => $this->credentials->getPayeeId(),
                    'amount' => $data['amount'],
                    'shopOrderNumber' => $data['shopOrderNumber'],
                    'expDate' => '20210217175959',
                    'billCurrency' => $data['currency'],
                    'preauthFlag' => 'N',
                    'successUrl' => $data['successUrl']
                ]
            ],
            'id' => '1'
        ];

        return $this;

    }

    /**
     * @return ResponseAbstractInterface
     */
    public function getResponse(): ResponseAbstractInterface
    {
        return $this->response;
    }


}