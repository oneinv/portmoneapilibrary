<?php

namespace PortmoneLib;

use GuzzleHttp\Client;

class RefundRequest implements RequestInterface
{

    public const METHOD = 'refund';

    private const API_URL = 'https://www.portmone.com.ua/gateway/';

    private $credentials;

    private $client;

    /**
     * RefundRequest constructor.
     * @param CredentialsInterface $credentials
     */
    public function __construct(CredentialsInterface $credentials)
    {
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);

        $this->credentials = $credentials;
    }

    public function sendRequest(bool $verbose): RequestInterface
    {
        // TODO: Implement sendRequest() method.
    }


    public function buildData(array $data): array
    {
        return [
            'method' => 'return',
            'params' => [
                'data' => [
                    'login' => $this->credentials->getLogin(),
                    'password' => $this->credentials->getPassword(),
                    'payeeId' => $this->credentials->getPayeeId(),
                    'shopOrderNumber' => $data['transactionName'],
                    'returnAmount' => $data['amount'],
                    'message' => 'empty message'
                ]
            ],
            'id' => '1'
        ];
    }

    public function fillRequest(array $data): RequestInterface
    {
        // TODO: Implement fillRequest() method.
    }

    public function getResponse(): ResponseAbstractInterface
    {
        // TODO: Implement getResponse() method.
    }


}