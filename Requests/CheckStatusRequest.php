<?php

namespace PortmoneLib;

use GuzzleHttp\Client;

class CheckStatusRequest implements RequestInterface
{
    public const METHOD = 'result';

    private const API_URL = 'https://www.portmone.com.ua/gateway/';

    /**
     * @var Client
     */
    private $client;
    /**
     * @var CredentialsInterface
     */
    private $credentials;

    /**
     * @var array
     */
    private $data;

    /**
     * @var ResponseAbstractInterface
     */
    private $response;

    /**
     * CheckStatusRequest constructor.
     * @param CredentialsInterface $credentials
     */
    public function __construct(CredentialsInterface $credentials)
    {
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);

        $this->credentials = $credentials;
    }

    /**
     * @param array $data
     * @return RequestInterface
     */
    public function fillRequest(array $data): RequestInterface
    {
        $this->data = [
            'method' => self::METHOD,
            'params' => [
                'data' => [
                    'login' => $this->credentials->getLogin(),
                    'password' => $this->credentials->getPassword(),
                    'payeeId' => $this->credentials->getPayeeId(),
                    'shopOrderNumber' => $data['shopOrderNumber'],
                    'expDate' => ''
                ]
            ],
            'id' => '1'
        ];

        return $this;
    }

    /**
     * @param bool $verbose
     * @return RequestInterface
     * @throws PortmoneException
     */
    public function sendRequest(bool $verbose): RequestInterface
    {
        $response = $this->client->post(
            self::API_URL,
            [
                'body' => json_encode($this->data)
            ]
        );

        $this->response = new CheckStatusResponse($response, $verbose);

        return $this;
    }

    /**
     * @return ResponseAbstractInterface
     */
    public function getResponse(): ResponseAbstractInterface
    {
        return $this->response;
    }



}