<?php


namespace PortmoneLib;


class Credentials implements CredentialsInterface
{
    /**
     * @var string
     */
    private $payeeId;
    /**
     * @var string
     */
    private $login;
    /**
     * @var string
     */
    private $password;

    /**
     * Credentials constructor.
     * @param string $payeeId
     * @param string $login
     * @param string $password
     */
    public function __construct(string $payeeId, string $login, string $password)
    {
        $this->payeeId = $payeeId;
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPayeeId(): string
    {
        return $this->payeeId;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getCredentials(): array
    {
        return [
            'payeeId'  => $this->getPayeeId(),
            'login'    => $this->getLogin(),
            'password' => $this->getPassword()
        ];
    }


}