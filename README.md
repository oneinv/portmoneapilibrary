# PortmoneApiLibrary

The main goal of this library is to receive data from **Portmone api gateway**


Currently library supports 2 api methods ***makeInvoice***, ***getInvoiceStatus***,  also implemented ***RefundRequest***.

Api methods constant store in Client.php. 

Library provides extend logs and errors by setting $verboseMode to ***true***.

Some examples: 

1 . ***Specify user Credentials***
```
$credentials = new Credentials($payeedId, $login, $password);
```
2 . ***Create Client***
```
$client = new Client($this->credentials, $verboseMode);
```

3 . ***Send Invoice Request***
```
$invoiceLink = $this->client
                  ->send(Client::METHOD_INVOICE, $this->data)
                  ->getInvoiceLink();
```

4 . ***Get Invoice Status***
```
$status = $this->client
            ->send(Client::METHOD_CHECK_STATUS, $this->data)
            ->getInvoiceStatus();
```
