<?php

namespace PortmoneLib;


class RequestAbstractFactory implements MethodFactory
{
    /**
     * @param string $method
     * @param CredentialsInterface $credentials
     * @return RequestInterface
     * @throws PortmoneException
     */
    public function createRequestObject(string $method, CredentialsInterface $credentials): RequestInterface
    {
        switch ($method) {
            case Client::METHOD_INVOICE:

                return new InvoiceRequest($credentials);
            case Client::METHOD_CHECK_STATUS:

                return new CheckStatusRequest($credentials);
            case Client::METHOD_REFUND:

                return new RefundRequest($credentials);
            default:

                throw new PortmoneException("Invalid api method \"{$method}\"");
        }
    }
}