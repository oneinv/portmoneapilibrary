<?php


namespace PortmoneLib;


class RefundResponse implements ResponseInterface
{
    /**
     * @var ResponseAbstractInterface
     */
    private $response;
    /**
     * @var bool
     */
    private $verbose;

    /**
     * RefundResponse constructor.
     * @param ResponseAbstractInterface $response
     * @param bool $verbose
     */
    public function __construct(ResponseAbstractInterface $response, bool $verbose)
    {
        $this->response = $response;
        $this->verbose = $verbose;
    }

    public function getStatusCode(): int
    {
        // TODO: Implement getStatusCode() method.
    }

    public function getExtendData(): \stdClass
    {
        // TODO: Implement getExtendData() method.
    }


}