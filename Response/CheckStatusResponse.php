<?php


namespace PortmoneLib;


class CheckStatusResponse implements ResponseInterface
{

    /**
     * @var ResponseAbstractInterface
     */
    private $response;

    /**
     * @var \stdClass
     */
    private $extendData;

    /**
     * @var int
     */
    private $statusCode;
    /**
     * @var string
     */
    private $invoiceStatus;

    /**
     * CheckStatusResponse constructor.
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param bool $verbose
     * @throws PortmoneException
     */
    public function __construct(\Psr\Http\Message\ResponseInterface $response, bool $verbose = false)
    {
        $this->response = $response;

        $this->statusCode = $response->getStatusCode();

        $this->invoiceStatus = $this->makeStatus();

        if ($verbose) {
            $this->fillExtendData();
        }
    }

    /**
     * @return string
     * @throws PortmoneException
     */
    public function makeStatus(): string
    {
        $responseData = json_decode($this->response->getBody());
        if (isset($responseData[0]->status)) {

            return $responseData[0]->status;
        }

        throw new PortmoneException('Failed to parse Response from Portmone');

    }

    /**
     * @return string
     */
    public function getInvoiceStatus(): string
    {
        return $this->invoiceStatus;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return \stdClass
     */
    public function getExtendData(): \stdClass
    {
        return $this->extendData;
    }


    private function fillExtendData(): void
    {
        $this->extendData = json_decode($this->response->getBody());
    }
}