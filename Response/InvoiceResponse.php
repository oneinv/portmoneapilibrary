<?php

namespace PortmoneLib;


class InvoiceResponse implements ResponseInterface
{
    /**
     * @var ResponseAbstractInterface
     */
    private $response;
    /**
     * @var \stdClass
     */
    private $extendData;

    /**
     * @var int
     */
    private $statusCode;
    /**
     * @var string
     */
    private $invoiceLink;

    /**
     * InvoiceResponse constructor.
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param bool $verbose
     * @throws PortmoneException
     */
    public function __construct(\Psr\Http\Message\ResponseInterface $response, bool $verbose = false)
    {
        $this->response = $response;

        $this->statusCode = $response->getStatusCode();

        $this->invoiceLink = $this->makeInvoiceLink();

        if ($verbose) {
            $this->fillExtendData();
        }
    }

    /**
     * @return string
     * @throws PortmoneException
     */
    private function makeInvoiceLink()
    {
        $responseData = json_decode($this->response->getBody());

        if (isset($responseData->result->linkInvoice)) {

            return $responseData->result->linkInvoice;
        }

        throw new PortmoneException('Failed to parse Response from Portmone');
    }

    /**
     * @return string
     */
    public function getInvoiceLink(): string
    {
        return $this->invoiceLink;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return \stdClass
     */
    public function getExtendData(): \stdClass
    {
        return $this->extendData;
    }


    private function fillExtendData(): void
    {
        $this->extendData = json_decode($this->response->getBody());
    }

}