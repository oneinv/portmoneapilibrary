<?php

namespace PortmoneLib;


class Client implements PaymentInterface
{
    public const METHOD_INVOICE = 'invoice';
    public const METHOD_CHECK_STATUS = 'checkStatus';
    public const METHOD_REFUND = 'refund';

    /**
     * @var RequestAbstractFactory
     */
    private $requestFactory;

    /**
     * @var CredentialsInterface
     */
    private $credentials;
    /**
     * @var bool
     */
    private $verbose;
    
    public function __construct(CredentialsInterface $credentials, bool $verbose = false)
    {
        $this->credentials = $credentials;
        $this->verbose = $verbose;

        $this->requestFactory = new RequestAbstractFactory();
    }

    /**
     * @param string $method
     * @param array $data
     * @return ResponseAbstractInterface
     * @throws PortmoneException
     */
    public function send(string $method, array $data): ResponseAbstractInterface
    {
        return  $this->requestFactory
            ->createRequestObject($method, $this->credentials)
            ->fillRequest($data)
            ->sendRequest($this->verbose)
            ->getResponse();
    }

}